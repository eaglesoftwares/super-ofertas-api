<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        "nome"
    ];

    public function ofertas(){
        return $this->hasMany('App\Oferta', 'empresa_id');
    }
}
