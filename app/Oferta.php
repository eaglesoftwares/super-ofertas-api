<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    protected $fillable = [
        "titulo", "descricao", "preco", "empresa_id"
    ];

    public function empresa(){
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }
}
