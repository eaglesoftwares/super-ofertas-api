<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Oferta;
use App\Empresa;
use Validator;

class OfertaController extends Controller
{
    protected function validarOferta($request){
        $validator = Validator::make($request->all(),[
            "titulo" => 'required',
            "descricao" => 'required',
            "preco" => 'required|numeric|',
            "empresa_id" => 'required'
            ]);
        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $ofertas = Oferta::paginate($qtd);
            
            $ofertas = $ofertas->appends(Request::capture()->except('page')); 

            return response()->json(['ofertas'=>$ofertas], 200);
        } catch (\Exception $e){
            return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = $this->validarOferta($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['titulo', 'descricao', 'preco', 'empresa_id']);
            if($data){
                $empresa = $data['empresa_id'];
                if(!Empresa::find($empresa)){
                    return response()->json(['message'=>'Empresa a ser relacionada não existe'], 404);                    
                }
                $oferta = Oferta::create($data);
                if($oferta){
                    return response()->json(['data'=> $oferta], 201);
                }else{
                    return response()->json(['message'=>'Erro ao criar a oferta', 'data'=> $oferta], 201);
                }
            }else{
                return response()->json(['message'=>'Dados inválidos'], 400);
            }     
            }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
            } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id < 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $oferta = Oferta::find($id);
            if($oferta){
                return response()->json([$oferta], 200);
            }else{
                return response()->json(['message'=>'A oferta com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validator = $this->validarOferta($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['titulo', 'descricao', 'preco', 'empresa_id']);
            if($data){
                $empresa = $data['empresa_id'];
                if(!Empresa::find($empresa)){
                    return response()->json(['message'=>'Empresa a ser relacionada não existe'], 404);                    
                }
                $oferta = Oferta::find($id);
                if($oferta){
                    $oferta->update($data);
                    return response()->json(['data'=> $oferta], 200);
                }else{
                    return response()->json(['message'=>'A oferta com id '.$id.' não existe'], 400);
                }
            }else{
                return response()->json(['message'=>'Dados inválidos'], 400);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if($id < 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $oferta = Oferta::find($id);
            if($oferta){
                $oferta->delete();
                return response()->json([], 204);
            }else{
                return response()->json(['message'=>'A oferta com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }
}
