<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('ofertas', 'OfertaController');
Route::get('/empresas/{id}/ofertas', 'EmpresaController@ofertas')->name('empresas.ofertas');
Route::resource('empresas', 'EmpresaController');